describe('You set the db password', () => {
  it('checks for the existence of the db password', () => {
    const mongo_pw = process.env.MPW || require('../mongo_pw.js').password;
    expect(mongo_pw).toBeDefined();
  })
})
